My personal website, hosting information about my experience, education, events, competitions, and writeups for things I've done.

Check it out at:
- https://bardificer.com
- https://bardificer.gitlab.io
