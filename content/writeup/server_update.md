---
title: "HomeLab Update && Where I've been"
description: "A short update on what I've been getting up to in the (almost) year since I last posted."
date: "2024-02-16"
categproes:
  - "Writeup"
tags:
  - "Homelab"
---

<meta property="og:image" content="/img/thumbnail.img" />
<meta name="twitter:image" content="/img/thumbnail.img" />

## So I've been gone
I know I don't really have a "following" here but as I give this website out to people who are interested, who are looking to hire me, and really just anyone because I love bardificer as a pun and identity, I thought I'd give some sort of update. Yeah it's been almost a year since I've posted on here but I've been doing stuff I promise.

I took my PMJR exam, and while I failed my first attempt the feedback I got was entirely on the report and not the technical findings themselves so I feel pretty good about that overall, and once I build up enough PTO I'll go for another attempt and really really dig deep knowing what I'm getting into this time.

I've been working on TryHackMe and HacktheBox Academy though their SOC paths (SOC1/2 THM, CDSA Training HTB) since I'm looking to more into a technically-focused position and finding those jobs has been a little rough with the current market and my resume and socials being 90% soft skills since I throw most of my projects out before they see the light of day.

Those who know me personally will tell you I'm a huge musician. I've gotten hired for a few musicals, and started doing composing work which takes up about 70% of my free time. I really need to start posting the things I make even if they're not the best that they can be.

And lastly I got a proper job. It's not exactly where I want to be right now, but I'm moving towards positions I'd rather be in. So if anyone has any openings for SOC Analysts / Security Engineers where you'd get me hands-on, I'll kill it. Anyway, onto a bit of my recent infra upgrades.

## The HomeLab
I have a homelab, it's an old Lenovo ThinkStation P700 with some mid-range specs. I got it specifically for virtualization so I have 64GB of ram and some 44 threads over the 2 CPUs so it works for what I need, though I'm looking to pick up another ram kit to double capacity. This has been my bread and butter for processing for a while. I use it for game servers, file hosting, video streaming, VPN, and every once in a while when I make an ungodly project in Blender I'll make a 18 core VM to cook through it. It's really been great and useful, but the biggest use is for security projects and learning. I started a little overhaul of my network, and thought I'd share a bit of how it looks. The biggest update I made has been integrating SIEM/SOAR into my non-security stuff, and making an virtualized AD network to let malware run a bit wild in, and for me to better learn corporate Microsoft ecosystems. Anything in *italics* is on-the-way, just not fully integrated.

```
=================================================================
| System     | Network | Service                                |
=================================================================
| VPN        | LAN     | Wireguard                              |
+------------+---------+----------------------------------------+
| Fedora     | LAN     | Docker (Wazuh - *TheHive* - *ELK*)     |
+------------+---------+----------------------------------------+
| Kali       | LAN     | For HTB/THM/internal testing           |
+------------+---------+----------------------------------------+
| FLARE VM   | MALNET  | Windows malware analysis               |
+------------+---------+----------------------------------------+
| REMnux     | MALNET  | Linux/network malware analysis         |
+------------+---------+----------------------------------------+
| Forensics  | MALNET  | Host/network forensics                 |
+------------+---------+----------------------------------------+
| ML DE      | MALNET  | Machine learning detection engineering |
+------------+---------+----------------------------------------+
| AD         | MALNET  | Simluated AD network server            |
+------------+---------+----------------------------------------+
| Win Client | MALNET  | Simulated AD network client            |
+------------+---------+----------------------------------------+
| Samba      | MALNET  | Simulated AD network samba server      |
+------------+---------+----------------------------------------+
| Graylog    | MALNET  | Simulated AD network SIEM              |
+------------+---------+----------------------------------------+
```

{{< figure src="/homelab/diagram.jpg" alt="!!! THIS IMAGE HAD AN ERROR !!!" >}}

Hope you foundt his interesting, I'll work more on posting what I actually do since I tend to do a lot more than I ever share.
