---
title: "Experience"
description: "Experiences that aren't necessarily linked to formal insitutions of education."
menu:
  main:
    weight: 1
    name: "Experience"
---


# Hands-On Experiences
### TryHackMe
TryHackMe is an online platform that teaches cyber security through short, gamified real-world labs. We have content for both complete beginners and seasoned hackers, incorporation guides and challenges to cater for different learning styles.

I have completed the Jr Penetration Tester course on TryHackMe and started working on the Offensive Pentesting course. Currently, I am ranked in the top 2% on the site and regularly interact with the community to help educate others who are working on problems and are stuck.

### HackTheBox
[Hack The Box](https://hackthebox.com) is a massive, online cybersecurity training platform, allowing individuals, companies, universities and all kinds of organizations around the world to level up their hacking skills. It’s a gamified online cybersecurity practice and training platform.

I’ve hit my peak global rank #432 back in June 2022. I took a break for a while, focusing more on learning different techniques and developing my skills further. Currently I’ve completed more than I ever have and reached higher rankings on the site, although competition is better so I’m only rank #605 but that’s climbing quickly. It’s a really good place to practice my skills, and with the addition of HackTheBox Academy, I’ve been developing my skills more with official training courses and a soon-to-come certification.

---

# Work Experience

### Cybersecurity Analyst
##### GlobalSecurityIQ | March 2021 -> Present
- Provide consulting and incident response as part of a third-party security team for many differen companies and individual clients.
- Work with clients to improve and manage their security posture, along with ensuring that purchased tools are used effectively and economically.

### Cybersecurity Analyst Intern
##### Calspan | May 2021 -> August 2021
- Created a security awareness training course to satisfy NIST 800-171/CMMC requirements.
- Monitored and maintained the security stance of the company with the following tools: WSUS, TrendMicro, Darktrace, Barracuda, Cisco Firepower, Lansweeper, Sophos Central, Nessus

### Network Security Instructor
##### University at Buffalo | August 2022 – Present

- Teach and write the Network Security course working with network virtualization, security lab design, packet capture analysis, and malware analysis report writing.

### Network Security Teaching Assistant
##### University at Buffalo | August 2020 – August 2022

- Teach the Network Security course working with network virtualization, security lab design, packet capture analysis, and malware analysis report writing.

### Lab Assistant
##### University at Buffalo | August 2019 – August 2020, August 2022 – Present

- Assist the design for a virtualization lab, fulfilling the requirements of professors’ classes and assignments.
- Test and validate virtualized labs for students to practice skills in information assurance and security audits.

