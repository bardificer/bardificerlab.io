---
title: "Education"
description: "My education, formal institutions and certifications. No independent practice, stuff that I was taught from somwhere."
menu:
  Main:
    name: "Education"
    weight: 1
---

# Certifications

## CompTIA PenTest+ (PTO-002)
#### July 2021 | exp. July 2024

An intermediate certification that ensures the knowledge and skills required to plan and scope a penetration testing engagement including vulnerability scanning, understand legal and compliance requirements, analyze results, and produce a written report with remediation techniques.

This offensive-focused certification is my first official step into offensive security specialization. It’s something I really enjoy, and while this certification is mostly knowledge-based, my performance outlined with my competitions and practical experiences show that I have practical skills. I know a lot of information about processes that penetration testers go through. This includes all the less-technical topics, like scoping, requirements planning, and writing an effective report. I plan to follow this certification with the eJPT Certification, a fully-practical intermediate penetration testing certification.

## CompTIA CySA+ (CS0-002)
#### January 2021 | exp. January 2027

An intermediate certification that ensures the knowledge and skills required to leverage intelligence and threat detection techniques, analyze and interpret data, identify and address vulnerabilities, suggest preventative measures, and effectively respond to and recover from incidents.

This defensive-focused certification was my first certification ever. I skipped over Security+ because the topics seemed too simple for me. Looking back on it, it would have been a good investment for making it past automated HR screening. Either way, my knowledge of defensive security operations and processes makes me a skilled analyst for cyber security.


# Formal Institutions

## B.S. Information Technology and Management | Cyber Security Minor
#### University at Buffalo | August 2019 – May 2023

The Information Technology and Management (ITM) degree focuses on both technical and business skills. It provides practical programming experience while developing critical soft skills through the curriculum’s strong management component and learn how to leverage technology to deliver results. Additionally, the Cyber Security Minor program helps foster more skills in the specific field, while offering me flexibility to pursue certifications and other experiences outside the traditional classroom environment.

While cybersecurity is a new and quickly changing discipline, a more versatile degree in both business and technology management helps round out my skill set. I’m learning about general business practices like marketing, finance, accounting, data management, and legal requirements of business while also developing technical skills. My major, combined with my minor and other certifications help me understand the type of work done by various employees and companies.


## NYS Regents Diploma with Advanced Designation with Honors
#### Williamsville South High School | September 2015 – June 2019

This diploma is representative of great performance in various subjects, covering math, science, English, history, and health. Additionally, NYS Honors includes many years of non-English language studies and arts experiences. In my time I was able to compete at a few high school cyber security competitions, play concerts to sold out audiences at my high school and Shea’s, and even earn the prestigious vote of “Most Musical Senior”.
