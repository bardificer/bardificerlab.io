---
title: "My Time at CPTC"
description: "My experience with the Collegiate Penetration Testing Competition."
categories:
  - "Competitions"
tags:
  - "Competition"
  - "Offense"
---
# Collegiate Penetration Testing Competition
#### 2019, 2020, 2021, 2022

The Collegiate Penetration Testing Competition (CPTC) is a very unique competition experience. In most competitions, the competing team is a “blue” team or defensive team, defending a network against attackers and completing tasks given to them. CPTC takes this whole idea and flips it on its head. This competition focuses on creating an environment similar to an actual penetration test, complete with RFP, client interactions, and C-Suite executives with less than ideal technological understanding. This competition takes a penetration test and puts it’s timeline into one day. Usually there’s somewhere about 6-10 hours of actual penetration test, and then another 4-8 hours of writing the report. This competition is probably my favorite to do because of how fun it is.

Every since year that I’ve been in college, I’ve represented University at Buffalo at CPTC. My freshman year, we made it past the regional competition and into the international level of competition. The next two years we placed just under the cutoff for internationals, but still put up good scores and performance. For those 3 years, I served the role of documentation/communication lead. During a penetration test, there’s a lot of information traveling around and it’s my job to deal with it. I spend most hours at the competition organizing information, guiding our efforts, and writing the penetration test reports. Over these years I found a few vulnerabilities, although I had limited time “on keyboard” before having to start on the report.

This year, 2022, I took a different role on the team. Instead, I was spearheading the actual penetration test. The competition isn’t over, so I can’t say too much about what I found, but I can say that I can tell my skills and knowledge have improved greatly. I had good findings, just if you’re gonna ask make sure that it’s in February. In the regional competition we placed 2nd. Even if that doesn’t qualify us for a spot at internationals, our team had 4 first-time competitors and we performed very well overall.

In the future, I plan to volunteer at CPTC and help my favorite competition run even better.
