---
title: "Competing at CCDC"
description: "My experience with the Collegiate Cyber Defense Competition."
categories:
  - "Competitions"
tags:
  - "Competition"
  - "Defense"
---
# Collegiate Cyber Defense Competition
#### 2020, 2021, 2022, 2023

The Collegiate Cyber Defense Competition (CCDC) provides a more typical experience to those competing in cyber security competitions. The competitors are the blue team, defending a network against active attacks, responding to and performing injects, and securing systems that are already post-exploitation phase. The mission is to “create a competitive environment to assess students’ depth of understanding and operational competency in managing the challenges inherent in protecting a corporate network infrastructure and business information”. Basically, it’s fun defensive cyber security.

Since my freshman year, I’ve represented the University at Buffalo at CCDC every competition. Each previous year, we’ve competed at both qualifying and regional competitions. My usual role in this competition is a network analyst, using a SIEM configuration and my knowledge of network intrusions to relay information to the rest of the team. While I’m the eyes on the network, other team members focus on individual tasks and system configurations. I also tie in with the team member focused on firewall configuration and management. In the past years, we’ve done well and have enjoyed the competition, growing skills and learning more about defensive infrastructure. This past year, 2023, I acted as the team captain for CCDC. I managed the rest of the team and kept them organized. It was a fun event, and everyone on the team learned quite a bit.
