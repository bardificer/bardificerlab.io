---
title: "(Don't) Hack The Port"
description: "My experience with Hack the Port."
categories:
  - "Competitions"
tags:
  - "Competition"
  - "Defense"
---
# Hack the Port – Gulfport
#### Summer 2022

This competition was a totally new one to the University at Buffalo team: Hack the Port. A nautical-themed “red vs blue” competition about defending critical infrastructure from attacks. This competition was a fun time (despite my computer breaking during the competition). We acted as a blue team during the competition, defending a port’s infrastructure from malicious attackers.

I know the rest of the team really enjoyed the competition, and want to look into more competitions hosted by Dreamport. In the future, I most likely won’t compete in these competitions. I’ve been a stable member of UB’s main competition team for cyber, but in these competitions that are more frequent (and aren’t the pinnacle “red” and “blue” competitions like CPTC and CCDC), I want to let younger and newer members have a chance to experience it. I’m thankful that I got to compete here, and I wish the future teams all the luck in the world.
