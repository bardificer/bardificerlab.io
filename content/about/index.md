---
title: "About Me"
description: "Read about who I am, and some of my accomplishments."
menu:
  main:
    weight: 1
---
## Who I Am

I'm a college student currently finishing up my senior year at the University at Buffalo. My studies are through the School of Management with a (projected) Bachelors in Information Technology and Management with a Minor in Cybersecurity. For 5 years I've been practicing, learning, and developing my skills to become a penetration tester.

### If You Really Want to Know More
Starting from a very young age, I was interested in computers. I started with Scratch and Python before I was out of elementary school. That moved into videogames, technology, hardware, software, and now to cybersecurity. In Eighth grade, I started working with cybersecurity though the UB GenCyber camp and since then I was sold.

Then, year after year, I returned to the camp. First two years as a student, then as a counselor in training for a few years, and now help out with planning, teaching, and running the camp. After getting into cyber, I started messing with more hardware, learning more programming, doing cybersecurity competitions, and taking classes. Now I help out with the camp, teach a class focused around network data analysis, and work part-time as a cybersecurity analyst.
